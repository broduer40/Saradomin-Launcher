namespace Saradomin.Model
{
    public class PluginInfo
    {
        public string Name { get; }

        public PluginInfo(string name)
        {
            Name = name;
        }
    }
}